export const typeDefs = `

type Post {
  id: Int
  title: String
  text: String
  authorId: Int
  author: Author
}

type Author {
  id: Int
  firstName: String
  lastName: String
  posts(limit: Int, offset: Int): [Post]
}

type Query {
  post(id: Int): [Post]
  author(id: Int): [Author]
}
`;