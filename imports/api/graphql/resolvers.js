import { Post, Author } from "./connectors"

export const resolvers = {
  Query: {
    async post(root, args, context) {
      return Post.findAll({ where: args.id, include: [Author]});
    },
    async author(root, args, context) {
      return Author.findAll({ where: args.id, include: [Post] });
    }
  }
}