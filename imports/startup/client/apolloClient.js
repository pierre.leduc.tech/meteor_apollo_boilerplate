import ApolloClient from 'apollo-client';
import { setup } from 'meteor/swydo:blaze-apollo';

// When using meteor/swydo:ddp-apollo:
import { DDPNetworkInterface } from 'meteor/swydo:ddp-apollo';
const client = new ApolloClient ({
  networkInterface: new DDPNetworkInterface()
});

console.log(client);
setup({ client });

export default client;