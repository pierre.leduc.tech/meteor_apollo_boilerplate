import { setup } from 'meteor/swydo:ddp-apollo';
import { makeExecutableSchema, addMockFunctionsToSchema } from 'graphql-tools';
import { PubSub, SubscriptionManager } from 'graphql-subscriptions';

import { typeDefs } from '/imports/api/graphql/schema';
import { resolvers } from '/imports/api/graphql/resolvers';

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const pubsub = new PubSub();

const subscriptionManager = new SubscriptionManager({
  schema,
  pubsub
});

const options = { subscriptionManager };
setup(schema, options);