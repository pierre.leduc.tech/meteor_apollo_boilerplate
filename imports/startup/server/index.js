// Import server startup through a single index entry point

import './apolloServer.js';
import './fixtures.js';
import './register-api.js';
