import './post.html';

import gql from 'graphql-tag';

const POSTS_QUERY = gql`
{
  post {
    id
    title
    author {
      firstName
      lastName
    }
  }
}
`

Template.post.helpers({
  posts() {
    return Template.instance().gqlQuery({ query: POSTS_QUERY }).get().post;
  },
});